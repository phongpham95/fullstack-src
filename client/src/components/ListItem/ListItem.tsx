import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { Location } from '../../graphql/types';
import Item from '../Item/Item';

const StyledContainer = styled.div`
  padding-top: 20px;
  text-align: center;
`;

const StyledListItem = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  @media only screen and (max-width: 768px) {
    width: 100%;
  }
  @media screen and (min-width: 992px) {
    padding: 0 10%;
  }
`;

const StyledTitle = styled.div`
  font-size: 22px;
  margin-bottom: 10px;
`;

const StyledLink = styled(Link)`
  display: block;
  font-size: 12px;
  text-decoration: underline;
`;

interface IProps {
  title?: string;
  link?: string;
  data: Location[];
}

const ListItem: FC<IProps> = ({ title, link, data }) => {
  return (
    <StyledContainer>
      <StyledTitle>
        {title}
        {link && <StyledLink to={link}>Xem thêm</StyledLink>}
      </StyledTitle>
      <StyledListItem>
        {data.map((item: Location) => (
          <Item key={item._id} item={item} />
        ))}
      </StyledListItem>
    </StyledContainer>
  );
};

export default ListItem;

import { Avatar, Badge, Dropdown, Icon, Layout, Menu } from 'antd';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import { useStores } from 'stores';
import styled from 'styled-components';

const StyledHeader = styled(Layout.Header)`
  padding: 0;
  background: #fff;
  display: flex;
  justify-content: space-between;
`;

const StyledTrigger = styled.div`
  padding: 0 24px;
  cursor: pointer;

  :hover {
    color: #1890ff;
  }
`;

const Header: FC = observer(() => {
  const {
    authStore: { logout },
    userStore: { currentUser },
    layoutStore: { isSiderCollapse, toggleSider, toggleShoppingCart },
    shoppingCartStore: { total }
  } = useStores();

  const isMobile = window.innerWidth < 768;

  const header = { marginLeft: 0 };

  if (isMobile && !isSiderCollapse) {
    header.marginLeft = 200;
  }

  const menu = (
    <Menu>
      <Menu.Item key="1">
        <Icon type="user" style={{ marginRight: 8 }} />
        Tài khoản
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="2" style={{ color: "red" }} onClick={() => logout()}>
        <Icon type="logout" style={{ marginRight: 8 }} />
        Thoát
      </Menu.Item>
    </Menu>
  );

  const admin = (
    <StyledTrigger>
      <Dropdown overlay={menu} placement="bottomRight">
        <Avatar>{get(currentUser, "name", "")[0]}</Avatar>
      </Dropdown>
    </StyledTrigger>
  );

  const user = (
    <StyledTrigger onClick={() => toggleShoppingCart()}>
      <Badge count={total.item} showZero={true} offset={[5, -5]}>
        <Icon type="shopping-cart" style={{ fontSize: 18 }} />
      </Badge>
    </StyledTrigger>
  );

  return (
    <StyledHeader style={{ ...header }}>
      <StyledTrigger onClick={() => toggleSider()}>
        <Icon type="menu" style={{ fontSize: 18 }} />
      </StyledTrigger>
      {currentUser ? admin : user}
    </StyledHeader>
  );
});

export default Header;

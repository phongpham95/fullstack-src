import { Icon, Layout, Menu } from 'antd';
import { observer } from 'mobx-react';
import React, { FC, useState } from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import { routes } from '../../screens/Routes';
import { useStores } from '../../stores';

const { Sider } = Layout;
const { Item, SubMenu } = Menu;

const StyledSider = styled(Sider)`
  height: 100vh;
  overflow: auto;
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1;
`;

const StyledMenu = styled(Menu)`
  line-height: 64;
`;

const StyledLogo = styled.div`
  height: 32px;
  background: rgba(255, 255, 255, 0.2);
  margin: 16px;
`;

const Navigation: FC<RouteComponentProps> = observer(() => {
  const {
    userStore: { currentUser },
    layoutStore: { isSiderCollapse, toggleSider }
  } = useStores();

  const [isMobile, setMobile] = useState(
    window.innerWidth < 768 ? true : false
  );

  return (
    <StyledSider
      collapsible
      trigger={null}
      collapsedWidth={isMobile ? 0 : 80}
      breakpoint="md"
      onBreakpoint={() => {
        setMobile(window.innerWidth < 768 ? true : false);
      }}
      onCollapse={collapse => {
        toggleSider(collapse);
      }}
      collapsed={isSiderCollapse}
    >
      <StyledLogo />
      <StyledMenu theme="dark" mode={isMobile ? "vertical" : "inline"}>
        {routes
          .filter(route => {
            if (!currentUser && route.isPrivate) {
              return false;
            }
            return true;
          })
          .map(route =>
            route.subRoutes ? (
              <SubMenu
                key={route.name}
                title={
                  <span>
                    <Icon type={route.icon} />
                    <span>{route.name}</span>
                  </span>
                }
              >
                {route.subRoutes.map(subRoute => (
                  <Item key={subRoute.name}>
                    <Link to={route.path + subRoute.path}>
                      <Icon type={subRoute.icon} />
                      <span>{subRoute.name}</span>
                    </Link>
                  </Item>
                ))}
              </SubMenu>
            ) : (
              <Item key={route.name}>
                <Link to={route.path}>
                  <Icon type={route.icon} />
                  <span>{route.name}</span>
                </Link>
              </Item>
            )
          )}
      </StyledMenu>
    </StyledSider>
  );
});

export default withRouter(Navigation);

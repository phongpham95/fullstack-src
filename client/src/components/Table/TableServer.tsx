import { Table } from 'antd';
import { PaginationConfig, TableProps } from 'antd/lib/table';
import { get } from 'lodash';
import React, { FC, useEffect, useState } from 'react';

const sortable = {
  ascend: "ASC",
  descend: "DESC"
};

interface IProps extends TableProps<any> {
  onQuery: (variable: any) => void;
  queryKey: string;
  onCount: (variable: any) => void;
  countKey: string;
  filterOption?: any;
}

const TableServer: FC<IProps> = ({
  onQuery,
  queryKey,
  onCount,
  countKey,
  filterOption,
  ...props
}) => {
  const [paginationOption, setPaginationOption] = useState<PaginationConfig>(
    {}
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    if (filterOption && filterOption.limit) {
      setPaginationOption({ pageSize: filterOption.limit, current: 1 });
    }
    fetchData(filterOption);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filterOption]);

  const fetchData = async (filter: any = {}) => {
    setLoading(true);

    const count = await onCount({ where: get(filter, "where", {}) });

    const total = count[countKey] | 0;
    setPaginationOption({
      ...paginationOption,
      total,
      showTotal: (total, range) => `${range[0]}-${range[1]} of ${total}`
    });

    if (total > 0) {
      const query = await onQuery({ ...filter });

      setData(query[queryKey]);
    } else {
      setData([]);
    }

    setLoading(false);
  };

  const onChange = async (pagination, _filters, sorter) => {
    const { current, pageSize } = pagination;
    const { field, order } = sorter;

    setPaginationOption({ ...paginationOption, current });

    const limit = pageSize;
    const skip = limit * (current - 1);

    let sort = {};
    if (field) {
      sort = { [field]: sortable[order] };
    }

    const filter = { ...filterOption, limit, skip, sort };

    await fetchData(filter);
  };

  return (
    <Table
      rowKey="_id"
      dataSource={data}
      pagination={paginationOption}
      loading={loading}
      onChange={onChange}
      {...props}
    />
  );
};

export default TableServer;

import { Button, Icon } from 'antd';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import ReactResizeDetector from 'react-resize-detector';
import styled from 'styled-components';

import { Location } from '../../graphql/types';
import { useStores } from '../../stores';
import { ILocationCart } from '../../stores/shoppingCartStore';

const StyledImgBox = styled.div`
  width: 100%;
  overflow: hidden;

  img {
    transition: transform 0.5s ease;
  }

  :hover img {
    transform: scale(1.2);
  }
`;

const StyledContainer = styled.div`
  padding: 10px;
  margin-bottom: 10px;
  text-align: center;

  :hover {
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  .item-content {
    padding: 10px;
  }

  .item-name {
    color: black;
    font-size: 20px;
  }

  .item-price {
    color: #5b5a5e;
    font-size: 18px;
  }

  @media only screen and (min-width: 1600px) {
    width: 20%;
  }

  @media only screen and (max-width: 1600px) {
    width: 25%;
  }

  @media only screen and (max-width: 1200px) {
    width: 33.33%;
  }

  @media only screen and (max-width: 576px) {
    width: 50%;
  }
`;

interface IProps {
  item: Location;
}

const Item: FC<IProps> = observer(({ item }) => {
  const {
    shoppingCartStore: { addItem }
  } = useStores();

  const location: ILocationCart = { ...item, amount: 1 };

  return (
    <StyledContainer>
      <ReactResizeDetector handleHeight={true}>
        {({ width }: any) => (
          <StyledImgBox style={{ height: width }}>
            <img
              alt={get(item, "name", "default")}
              src={get(
                item,
                "images[0].path",
                require("../../assets/images/404.png")
              )}
              width="100%"
            />
          </StyledImgBox>
        )}
      </ReactResizeDetector>
      <div className="item-content">
        <div className="item-name">{get(item, "name", "")}</div>
        <div className="item-price">{`$ ${get(item, "categoryId", "")}`}</div>
      </div>
      <Button
        type="primary"
        onClick={() => addItem(location)}
        block={true}
        size="large"
      >
        <Icon type="shopping-cart" />
      </Button>
    </StyledContainer>
  );
});

export default Item;

import { GraphQLClient } from 'graphql-request';
import { print } from 'graphql';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any,
};

export type BaseModel = {
  _id: Scalars['ID'],
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
};

export type DateOperatorArgs = {
  gt?: Maybe<Scalars['DateTime']>,
  gte?: Maybe<Scalars['DateTime']>,
  lt?: Maybe<Scalars['DateTime']>,
  lte?: Maybe<Scalars['DateTime']>,
};


export type File = {
  id: Scalars['String'],
  path: Scalars['String'],
};

export type FilePayload = {
  data?: Maybe<File>,
  error?: Maybe<StandardMutationError>,
};

export type Location = {
  _id: Scalars['ID'],
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  name: Scalars['String'],
  address: Scalars['String'],
  categoryId: Scalars['String'],
  imageIds?: Maybe<Array<Scalars['String']>>,
  creatorId?: Maybe<Scalars['ID']>,
  customerId?: Maybe<Scalars['ID']>,
  customer?: Maybe<User>,
  images?: Maybe<Array<File>>,
  creator?: Maybe<User>,
};

export type LocationBasePayload = {
  data?: Maybe<Location>,
  error?: Maybe<StandardMutationError>,
};

export type LocationFilter = {
  _ids?: Maybe<Array<Scalars['ID']>>,
  AND?: Maybe<Array<LocationFilter>>,
  OR?: Maybe<Array<LocationFilter>>,
  name?: Maybe<Scalars['String']>,
  categoryId?: Maybe<Scalars['String']>,
  creatorId?: Maybe<Scalars['ID']>,
  customerId?: Maybe<Scalars['ID']>,
  _id?: Maybe<Scalars['ID']>,
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  _operators?: Maybe<LocationFilterOperator>,
};

export type LocationFilterOperator = {
  name?: Maybe<StringOperatorArgs>,
  categoryId?: Maybe<StringOperatorArgs>,
  createdAt?: Maybe<DateOperatorArgs>,
  updatedAt?: Maybe<DateOperatorArgs>,
};

export type LocationInput = {
  name: Scalars['String'],
  address: Scalars['String'],
  categoryId: Scalars['String'],
  imageIds?: Maybe<Array<Scalars['String']>>,
  creatorId?: Maybe<Scalars['ID']>,
  customerId?: Maybe<Scalars['ID']>,
};

export type LocationSort = {
  name?: Maybe<SortDirection>,
  categoryId?: Maybe<SortDirection>,
  creatorId?: Maybe<SortDirection>,
  customerId?: Maybe<SortDirection>,
  createdAt?: Maybe<SortDirection>,
  updatedAt?: Maybe<SortDirection>,
};

export type LocationUpdateArg = {
  name?: Maybe<Scalars['String']>,
  address?: Maybe<Scalars['String']>,
  categoryId?: Maybe<Scalars['String']>,
  imageIds?: Maybe<Array<Scalars['String']>>,
};

export type LoginPayload = {
  data?: Maybe<User>,
  error?: Maybe<StandardMutationError>,
  authToken?: Maybe<Scalars['String']>,
};

export type Mutation = {
  removeFileById: FilePayload,
  login: LoginPayload,
  createUser: UserPayload,
  changePassword: UserPayload,
  createLocation: LocationBasePayload,
  updateLocationById: LocationBasePayload,
  removeLocationById: LocationBasePayload,
  createProfile: ProfileBasePayload,
  updateProfileById: ProfileBasePayload,
  removeProfileById: ProfileBasePayload,
  updateUserById: UserBasePayload,
  removeUserById: UserBasePayload,
  createFile: FilePayload,
};


export type MutationRemoveFileByIdArgs = {
  id: Scalars['String']
};


export type MutationLoginArgs = {
  password: Scalars['String'],
  email: Scalars['String']
};


export type MutationCreateUserArgs = {
  record: UserInput
};


export type MutationChangePasswordArgs = {
  newPassword: Scalars['String'],
  oldPassword: Scalars['String']
};


export type MutationCreateLocationArgs = {
  record: LocationInput
};


export type MutationUpdateLocationByIdArgs = {
  _id: Scalars['String'],
  record: LocationUpdateArg
};


export type MutationRemoveLocationByIdArgs = {
  _id: Scalars['String']
};


export type MutationCreateProfileArgs = {
  record: ProfileInput
};


export type MutationUpdateProfileByIdArgs = {
  _id: Scalars['String'],
  record: ProfileUpdateArg
};


export type MutationRemoveProfileByIdArgs = {
  _id: Scalars['String']
};


export type MutationUpdateUserByIdArgs = {
  _id: Scalars['String'],
  record: UserUpdateArg
};


export type MutationRemoveUserByIdArgs = {
  _id: Scalars['String']
};

export type NumberOperatorArgs = {
  in?: Maybe<Array<Scalars['Float']>>,
  nin?: Maybe<Array<Scalars['Float']>>,
  gt?: Maybe<Scalars['Float']>,
  gte?: Maybe<Scalars['Float']>,
  lt?: Maybe<Scalars['Float']>,
  lte?: Maybe<Scalars['Float']>,
};

export type Owner = {
  _id: Scalars['ID'],
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  userId: Scalars['String'],
  resourceId: Scalars['String'],
  model: Scalars['String'],
};

export type OwnerInput = {
  userId: Scalars['String'],
  resourceId: Scalars['String'],
  model: Scalars['String'],
};

export type Profile = {
  _id: Scalars['ID'],
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  profileId: Scalars['String'],
  display: Scalars['String'],
  description?: Maybe<Scalars['String']>,
  roles: Array<Scalars['String']>,
};

export type ProfileBasePayload = {
  data?: Maybe<Profile>,
  error?: Maybe<StandardMutationError>,
};

export type ProfileFilter = {
  _ids?: Maybe<Array<Scalars['ID']>>,
  AND?: Maybe<Array<ProfileFilter>>,
  OR?: Maybe<Array<ProfileFilter>>,
  profileId?: Maybe<Scalars['String']>,
  display?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['ID']>,
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  _operators?: Maybe<ProfileFilterOperator>,
};

export type ProfileFilterOperator = {
  profileId?: Maybe<StringOperatorArgs>,
  display?: Maybe<StringOperatorArgs>,
  createdAt?: Maybe<DateOperatorArgs>,
  updatedAt?: Maybe<DateOperatorArgs>,
};

export type ProfileInput = {
  profileId: Scalars['String'],
  display: Scalars['String'],
  description?: Maybe<Scalars['String']>,
  roles: Array<Scalars['String']>,
};

export type ProfileSort = {
  profileId?: Maybe<SortDirection>,
  display?: Maybe<SortDirection>,
  createdAt?: Maybe<SortDirection>,
  updatedAt?: Maybe<SortDirection>,
};

export type ProfileUpdateArg = {
  profileId?: Maybe<Scalars['String']>,
  display?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  roles?: Maybe<Array<Scalars['String']>>,
};

export type Query = {
  me: UserPayload,
  findLocationById?: Maybe<Location>,
  findOneLocation?: Maybe<Location>,
  findManyLocation: Array<Location>,
  countLocation?: Maybe<Scalars['Int']>,
  findProfileById?: Maybe<Profile>,
  findOneProfile?: Maybe<Profile>,
  findManyProfile: Array<Profile>,
  countProfile?: Maybe<Scalars['Int']>,
  findRoleById?: Maybe<Role>,
  findOneRole?: Maybe<Role>,
  findManyRole: Array<Role>,
  countRole?: Maybe<Scalars['Int']>,
  findUserById?: Maybe<User>,
  findOneUser?: Maybe<User>,
  findManyUser: Array<User>,
  countUser?: Maybe<Scalars['Int']>,
  findFileById: FilePayload,
};


export type QueryFindLocationByIdArgs = {
  _id: Scalars['String']
};


export type QueryFindOneLocationArgs = {
  where?: Maybe<LocationFilter>,
  sort?: Maybe<LocationSort>
};


export type QueryFindManyLocationArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  where?: Maybe<LocationFilter>,
  sort?: Maybe<LocationSort>
};


export type QueryCountLocationArgs = {
  where?: Maybe<LocationFilter>
};


export type QueryFindProfileByIdArgs = {
  _id: Scalars['String']
};


export type QueryFindOneProfileArgs = {
  where?: Maybe<ProfileFilter>,
  sort?: Maybe<ProfileSort>
};


export type QueryFindManyProfileArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  where?: Maybe<ProfileFilter>,
  sort?: Maybe<ProfileSort>
};


export type QueryCountProfileArgs = {
  where?: Maybe<ProfileFilter>
};


export type QueryFindRoleByIdArgs = {
  _id: Scalars['String']
};


export type QueryFindOneRoleArgs = {
  where?: Maybe<RoleFilter>,
  sort?: Maybe<RoleSort>
};


export type QueryFindManyRoleArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  where?: Maybe<RoleFilter>,
  sort?: Maybe<RoleSort>
};


export type QueryCountRoleArgs = {
  where?: Maybe<RoleFilter>
};


export type QueryFindUserByIdArgs = {
  _id: Scalars['String']
};


export type QueryFindOneUserArgs = {
  where?: Maybe<UserFilter>,
  sort?: Maybe<UserSort>
};


export type QueryFindManyUserArgs = {
  skip?: Maybe<Scalars['Int']>,
  limit?: Maybe<Scalars['Int']>,
  where?: Maybe<UserFilter>,
  sort?: Maybe<UserSort>
};


export type QueryCountUserArgs = {
  where?: Maybe<UserFilter>
};


export type QueryFindFileByIdArgs = {
  id: Scalars['String']
};

export type Role = {
  _id: Scalars['ID'],
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  roleId: Scalars['String'],
  description?: Maybe<Scalars['String']>,
};

export type RoleBasePayload = {
  data?: Maybe<Role>,
  error?: Maybe<StandardMutationError>,
};

export type RoleFilter = {
  _ids?: Maybe<Array<Scalars['ID']>>,
  AND?: Maybe<Array<RoleFilter>>,
  OR?: Maybe<Array<RoleFilter>>,
  roleId?: Maybe<Scalars['String']>,
  _id?: Maybe<Scalars['ID']>,
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  _operators?: Maybe<RoleFilterOperator>,
};

export type RoleFilterOperator = {
  roleId?: Maybe<StringOperatorArgs>,
  createdAt?: Maybe<DateOperatorArgs>,
  updatedAt?: Maybe<DateOperatorArgs>,
};

export type RoleSort = {
  roleId?: Maybe<SortDirection>,
  createdAt?: Maybe<SortDirection>,
  updatedAt?: Maybe<SortDirection>,
};

export type RoleUpdateArg = {
  roleId?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
};

export enum SortDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type StandardMutationError = {
  message: Scalars['String'],
  title?: Maybe<Scalars['String']>,
};

export type StringOperatorArgs = {
  in?: Maybe<Array<Scalars['String']>>,
  nin?: Maybe<Array<Scalars['String']>>,
};

export type User = {
  _id: Scalars['ID'],
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  name: Scalars['String'],
  email: Scalars['String'],
  phone?: Maybe<Scalars['String']>,
  password: Scalars['String'],
  profileId: Scalars['String'],
  creatorId?: Maybe<Scalars['ID']>,
  customerId?: Maybe<Scalars['ID']>,
  profile: Profile,
  creator?: Maybe<User>,
  customer?: Maybe<User>,
};

export type UserBasePayload = {
  data?: Maybe<User>,
  error?: Maybe<StandardMutationError>,
};

export type UserFilter = {
  _ids?: Maybe<Array<Scalars['ID']>>,
  AND?: Maybe<Array<UserFilter>>,
  OR?: Maybe<Array<UserFilter>>,
  name?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  phone?: Maybe<Scalars['String']>,
  profileId?: Maybe<Scalars['String']>,
  creatorId?: Maybe<Scalars['ID']>,
  customerId?: Maybe<Scalars['ID']>,
  _id?: Maybe<Scalars['ID']>,
  createdAt?: Maybe<Scalars['DateTime']>,
  updatedAt?: Maybe<Scalars['DateTime']>,
  _operators?: Maybe<UserFilterOperator>,
};

export type UserFilterOperator = {
  name?: Maybe<StringOperatorArgs>,
  email?: Maybe<StringOperatorArgs>,
  phone?: Maybe<StringOperatorArgs>,
  profileId?: Maybe<StringOperatorArgs>,
  createdAt?: Maybe<DateOperatorArgs>,
  updatedAt?: Maybe<DateOperatorArgs>,
};

export type UserInput = {
  name: Scalars['String'],
  email: Scalars['String'],
  phone?: Maybe<Scalars['String']>,
  password: Scalars['String'],
  profileId: Scalars['String'],
  creatorId?: Maybe<Scalars['ID']>,
  customerId?: Maybe<Scalars['ID']>,
};

export type UserPayload = {
  data?: Maybe<User>,
  error?: Maybe<StandardMutationError>,
};

export type UserSort = {
  name?: Maybe<SortDirection>,
  email?: Maybe<SortDirection>,
  phone?: Maybe<SortDirection>,
  profileId?: Maybe<SortDirection>,
  creatorId?: Maybe<SortDirection>,
  customerId?: Maybe<SortDirection>,
  createdAt?: Maybe<SortDirection>,
  updatedAt?: Maybe<SortDirection>,
};

export type UserUpdateArg = {
  name?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  phone?: Maybe<Scalars['String']>,
  profileId?: Maybe<Scalars['String']>,
};

export type LocationFragment = (
  Pick<Location, '_id' | 'name' | 'address' | 'categoryId' | 'imageIds' | 'creatorId' | 'customerId' | 'createdAt' | 'updatedAt'>
  & { images: Maybe<Array<Pick<File, 'id' | 'path'>>>, creator: Maybe<Pick<User, 'name' | 'email'>>, customer: Maybe<Pick<User, 'name' | 'email'>> }
);

export type UserFragment = (
  Pick<User, '_id' | 'name' | 'password' | 'email' | 'profileId' | 'createdAt' | 'updatedAt'>
  & { profile: Pick<Profile, '_id' | 'profileId' | 'display' | 'description' | 'roles'> }
);

export type CreateFileMutationVariables = {};


export type CreateFileMutation = { createFile: { data: Maybe<Pick<File, 'id' | 'path'>>, error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> } };

export type CreateLocationMutationVariables = {
  record: LocationInput
};


export type CreateLocationMutation = { createLocation: { data: Maybe<LocationFragment>, error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> } };

export type LoginMutationVariables = {
  email: Scalars['String'],
  password: Scalars['String']
};


export type LoginMutation = { login: (
    Pick<LoginPayload, 'authToken'>
    & { error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> }
  ) };

export type RemoveFileByIdMutationVariables = {
  id: Scalars['String']
};


export type RemoveFileByIdMutation = { removeFileById: { data: Maybe<Pick<File, 'id'>>, error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> } };

export type RemoveLocationByIdMutationVariables = {
  _id: Scalars['String']
};


export type RemoveLocationByIdMutation = { removeLocationById: { data: Maybe<LocationFragment>, error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> } };

export type UpdateLocationByIdMutationVariables = {
  _id: Scalars['String'],
  record: LocationUpdateArg
};


export type UpdateLocationByIdMutation = { updateLocationById: { data: Maybe<LocationFragment>, error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> } };

export type CountLocationQueryVariables = {
  where?: Maybe<LocationFilter>
};


export type CountLocationQuery = Pick<Query, 'countLocation'>;

export type FindFileByIdQueryVariables = {
  id: Scalars['String']
};


export type FindFileByIdQuery = { findFileById: { data: Maybe<Pick<File, 'id' | 'path'>>, error: Maybe<Pick<StandardMutationError, 'title' | 'message'>> } };

export type FindManyLocationQueryVariables = {
  where?: Maybe<LocationFilter>,
  limit?: Maybe<Scalars['Int']>,
  skip?: Maybe<Scalars['Int']>,
  sort?: Maybe<LocationSort>
};


export type FindManyLocationQuery = { findManyLocation: Array<LocationFragment> };

export type MeQueryVariables = {};


export type MeQuery = { me: { data: Maybe<UserFragment> } };

export const LocationFragmentDoc = gql`
    fragment Location on Location {
  _id
  name
  address
  categoryId
  imageIds
  images {
    id
    path
  }
  creatorId
  creator {
    name
    email
  }
  customerId
  customer {
    name
    email
  }
  createdAt
  updatedAt
}
    `;
export const UserFragmentDoc = gql`
    fragment User on User {
  _id
  name
  password
  email
  profileId
  profile {
    _id
    profileId
    display
    description
    roles
  }
  createdAt
  updatedAt
}
    `;
export const CreateFileDocument = gql`
    mutation CreateFile {
  createFile {
    data {
      id
      path
    }
    error {
      title
      message
    }
  }
}
    `;
export const CreateLocationDocument = gql`
    mutation CreateLocation($record: LocationInput!) {
  createLocation(record: $record) {
    data {
      ...Location
    }
    error {
      title
      message
    }
  }
}
    ${LocationFragmentDoc}`;
export const LoginDocument = gql`
    mutation Login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    authToken
    error {
      title
      message
    }
  }
}
    `;
export const RemoveFileByIdDocument = gql`
    mutation RemoveFileById($id: String!) {
  removeFileById(id: $id) {
    data {
      id
    }
    error {
      title
      message
    }
  }
}
    `;
export const RemoveLocationByIdDocument = gql`
    mutation RemoveLocationById($_id: String!) {
  removeLocationById(_id: $_id) {
    data {
      ...Location
    }
    error {
      title
      message
    }
  }
}
    ${LocationFragmentDoc}`;
export const UpdateLocationByIdDocument = gql`
    mutation UpdateLocationById($_id: String!, $record: LocationUpdateArg!) {
  updateLocationById(_id: $_id, record: $record) {
    data {
      ...Location
    }
    error {
      title
      message
    }
  }
}
    ${LocationFragmentDoc}`;
export const CountLocationDocument = gql`
    query CountLocation($where: LocationFilter) {
  countLocation(where: $where)
}
    `;
export const FindFileByIdDocument = gql`
    query FindFileById($id: String!) {
  findFileById(id: $id) {
    data {
      id
      path
    }
    error {
      title
      message
    }
  }
}
    `;
export const FindManyLocationDocument = gql`
    query FindManyLocation($where: LocationFilter, $limit: Int, $skip: Int, $sort: LocationSort) {
  findManyLocation(where: $where, limit: $limit, skip: $skip, sort: $sort) {
    ...Location
  }
}
    ${LocationFragmentDoc}`;
export const MeDocument = gql`
    query Me {
  me {
    data {
      ...User
    }
  }
}
    ${UserFragmentDoc}`;
export function getSdk(client: GraphQLClient) {
  return {
    CreateFile(variables?: CreateFileMutationVariables): Promise<CreateFileMutation> {
      return client.request<CreateFileMutation>(print(CreateFileDocument), variables);
    },
    CreateLocation(variables: CreateLocationMutationVariables): Promise<CreateLocationMutation> {
      return client.request<CreateLocationMutation>(print(CreateLocationDocument), variables);
    },
    Login(variables: LoginMutationVariables): Promise<LoginMutation> {
      return client.request<LoginMutation>(print(LoginDocument), variables);
    },
    RemoveFileById(variables: RemoveFileByIdMutationVariables): Promise<RemoveFileByIdMutation> {
      return client.request<RemoveFileByIdMutation>(print(RemoveFileByIdDocument), variables);
    },
    RemoveLocationById(variables: RemoveLocationByIdMutationVariables): Promise<RemoveLocationByIdMutation> {
      return client.request<RemoveLocationByIdMutation>(print(RemoveLocationByIdDocument), variables);
    },
    UpdateLocationById(variables: UpdateLocationByIdMutationVariables): Promise<UpdateLocationByIdMutation> {
      return client.request<UpdateLocationByIdMutation>(print(UpdateLocationByIdDocument), variables);
    },
    CountLocation(variables?: CountLocationQueryVariables): Promise<CountLocationQuery> {
      return client.request<CountLocationQuery>(print(CountLocationDocument), variables);
    },
    FindFileById(variables: FindFileByIdQueryVariables): Promise<FindFileByIdQuery> {
      return client.request<FindFileByIdQuery>(print(FindFileByIdDocument), variables);
    },
    FindManyLocation(variables?: FindManyLocationQueryVariables): Promise<FindManyLocationQuery> {
      return client.request<FindManyLocationQuery>(print(FindManyLocationDocument), variables);
    },
    Me(variables?: MeQueryVariables): Promise<MeQuery> {
      return client.request<MeQuery>(print(MeDocument), variables);
    }
  };
}
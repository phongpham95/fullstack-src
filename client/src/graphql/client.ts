import { GraphQLClient } from 'graphql-request';

const url = "http://localhost:8080/graphql";

const graphQLClient = new GraphQLClient(url);

const token = window.localStorage.getItem("token");

graphQLClient.setHeader("authorization", `Bearer ${token}`);

export default graphQLClient;

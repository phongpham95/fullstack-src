import gql from 'graphql-tag';

import { LOCATION_FRAGMENT } from '../fragments/Location';

export const UPDATE_LOCATION_BY_ID = gql`
  mutation UpdateLocationById($_id: String!, $record: LocationUpdateArg!) {
    updateLocationById(_id: $_id, record: $record) {
      data {
        ...Location
      }
      error {
        title
        message
      }
    }
  }
  ${LOCATION_FRAGMENT}
`;

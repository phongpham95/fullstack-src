import gql from 'graphql-tag';

import { LOCATION_FRAGMENT } from '../fragments/Location';

export const CREATE_LOCATION = gql`
  mutation CreateLocation($record: LocationInput!) {
    createLocation(record: $record) {
      data {
        ...Location
      }
      error {
        title
        message
      }
    }
  }
  ${LOCATION_FRAGMENT}
`;

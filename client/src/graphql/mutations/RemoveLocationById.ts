import gql from 'graphql-tag';

import { LOCATION_FRAGMENT } from '../fragments/Location';

export const REMOVE_LOCATION_BY_ID = gql`
  mutation RemoveLocationById($_id: String!) {
    removeLocationById(_id: $_id) {
      data {
        ...Location
      }
      error {
        title
        message
      }
    }
  }
  ${LOCATION_FRAGMENT}
`;

import client from './client';
import { getSdk } from './types';

export default getSdk(client);

import gql from 'graphql-tag';

export const COUNT_LOCATION = gql`
  query CountLocation($where: LocationFilter) {
    countLocation(where: $where)
  }
`;

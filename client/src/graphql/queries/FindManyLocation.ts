import gql from 'graphql-tag';

import { LOCATION_FRAGMENT } from '../fragments/Location';

export const FIND_MANY_LOCATION = gql`
  query FindManyLocation(
    $where: LocationFilter
    $limit: Int
    $skip: Int
    $sort: LocationSort
  ) {
    findManyLocation(where: $where, limit: $limit, skip: $skip, sort: $sort) {
      ...Location
    }
  }
  ${LOCATION_FRAGMENT}
`;

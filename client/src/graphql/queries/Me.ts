import gql from 'graphql-tag';

import { USER_FRAGMENT } from '../fragments/User';

export const ME = gql`
  query Me {
    me {
      data {
        ...User
      }
    }
  }
  ${USER_FRAGMENT}
`;

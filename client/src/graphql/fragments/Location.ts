import gql from 'graphql-tag';

export const LOCATION_FRAGMENT = gql`
  fragment Location on Location {
    _id
    name
    address
    categoryId
    imageIds
    images {
      id
      path
    }
    creatorId
    creator {
      name
      email
    }
    customerId
    customer {
      name
      email
    }
    createdAt
    updatedAt
  }
`;

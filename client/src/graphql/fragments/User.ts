import gql from 'graphql-tag';

export const USER_FRAGMENT = gql`
  fragment User on User {
    _id
    name
    password
    email
    profileId
    profile {
      _id
      profileId
      display
      description
      roles
    }
    createdAt
    updatedAt
  }
`;

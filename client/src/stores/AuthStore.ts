import { action, observable } from 'mobx';

import { ErrorNoti, SuccessNoti } from '../components/UI';
import client from '../graphql/sdk';
import commonStore from './commonStore';
import userStore from './userStore';

export class AuthStore {
  @observable
  inProgress: boolean = false;

  @action
  login = async (email: string, password: string) => {
    this.inProgress = true;

    const {
      login: { authToken, error }
    } = await client.Login({
      email,
      password
    });

    if (error) {
      ErrorNoti(error.message);
    } else {
      commonStore.setToken(authToken);
      await userStore.getUser();
      SuccessNoti("Đăng nhập");
      this.inProgress = false;
    }
  };

  @action
  logout = () => {
    commonStore.setToken(null);
    userStore.forgetUser();
    ErrorNoti("Hết phiên đăng nhập!");
    return window.location.reload();
  };
}

export default new AuthStore();

import { get } from 'lodash';
import { action, computed, observable } from 'mobx';

import { ME } from '../graphql/queries/Me';
import client from '../graphql/sdk';
import authStore from './authStore';

class UserStore {
  @observable
  currentUser: any = null;

  @observable
  loadingUser: boolean = false;

  @observable
  updatingUser: boolean = false;

  @computed
  get roles() {
    return this.currentUser && get(this.currentUser, "profile.roles");
  }

  @action
  getUser = async () => {
    this.loadingUser = true;
    const {
      me: { data }
    } = await client.Me({
      query: ME
    });

    if (!data) {
      authStore.logout();
    } else {
      this.currentUser = data;
      this.loadingUser = false;
    }
  };

  @action forgetUser() {
    this.currentUser = undefined;
  }
}

export default new UserStore();

import { action, computed, observable } from 'mobx';

import { Location } from '../graphql/types';

export interface ILocationCart extends Location {
  amount: number;
}

export class ShoppingCartStore {
  @observable
  cart: ILocationCart[] = [];

  @computed
  get total() {
    const item = this.cart.reduce((all, item) => {
      all += item.amount;
      return all;
    }, 0);
    const price = this.cart.reduce((all, item) => {
      all += 10 * item.amount;
      return all;
    }, 0);
    return { item, price };
  }

  @action
  addItem = (product: ILocationCart, amount: number = 1) => {
    const index = this.cart.findIndex(item => item._id === product._id);

    if (index > -1) {
      const total = (this.cart[index].amount += amount);

      if (total > 0) {
        this.cart = [
          ...this.cart.slice(0, index),
          {
            ...product,
            amount: total
          },
          ...this.cart.slice(index + 1)
        ];
      } else {
        this.removeItem(product);
      }
    } else {
      this.cart = [
        ...(this.cart || []),
        {
          ...product,
          amount: 1
        }
      ];
    }
  };

  @action
  removeItem = (location: ILocationCart) => {
    this.cart = this.cart.filter(item => item._id !== location._id);
  };
}

export default new ShoppingCartStore();

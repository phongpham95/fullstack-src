import { Button, Form, Icon, Input } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { observer } from 'mobx-react';
import React, { FC, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router';
import styled from 'styled-components';

import { useStores } from '../../stores';

const { Item } = Form;
const { Password } = Input;

const StyledIcon = styled(Icon)`
  color: rgba(0, 0, 0, 0.25);
`;

const Container = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  height: 100vh;
`;

const LoginForm = styled.div`
  margin-top: 20%;
  text-align: center;
  background: #000;
  padding: 25px;
  border-radius: 15px;
  background: #00152a;
`;

const Login: FC<FormComponentProps> = observer(({ form }) => {
  const {
    authStore: { login },
    userStore: { currentUser }
  } = useStores();

  const history = useHistory();
  const location = useLocation();

  const { from } = location.state || { from: { pathname: "/" } };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    await form.validateFields(async (err, { email, password }) => {
      if (!err) {
        await login(email, password);
      }
    });
  };

  useEffect(() => {
    if (currentUser) {
      history.replace(from);
    }
  });

  const { getFieldDecorator } = form;

  return (
    <Container>
      <LoginForm>
        <h1 style={{ color: "#fff" }}>Đăng nhập</h1>
        <Form onSubmit={e => handleSubmit(e)}>
          <Item>
            {getFieldDecorator("email", {
              rules: [
                { required: true, message: "Vui lòng nhập tên đăng nhập!" }
              ]
            })(
              <Input prefix={<StyledIcon type="user" />} placeholder="Email" />
            )}
          </Item>
          <Item>
            {getFieldDecorator("password", {
              rules: [{ required: true, message: "Vui lòng nhập mật khẩu!" }]
            })(
              <Password
                prefix={<StyledIcon type="lock" />}
                type="password"
                placeholder="Mật khẩu"
              />
            )}
          </Item>
          <Item>
            <Button type="primary" block={true} icon="login" htmlType="submit">
              Đăng nhập
            </Button>
          </Item>
        </Form>
      </LoginForm>
    </Container>
  );
});

export default Form.create()(Login);

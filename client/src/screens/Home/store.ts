import { action, observable } from 'mobx';

import client from '../../graphql/sdk';
import { Location, LocationFilter, LocationSort } from '../../graphql/types';

export class HomeStore {
  @observable
  loading: boolean = true;

  @observable
  data: Location[] = [];

  initialized: boolean = false;

  constructor() {
    this.init(true);
  }

  @action
  init = async (forceReinit: boolean = false) => {
    if (this.initialized && !forceReinit) {
      return;
    }
    await this.fetchData().finally(
      action(() => {
        this.loading = false;
        this.initialized = true;
      })
    );
  };

  @action
  fetchData = async (
    where?: LocationFilter,
    limit?: number,
    skip?: number,
    sort?: LocationSort
  ) => {
    const width = window.innerWidth;

    limit = 6;
    if (1200 < width && width < 1600) {
      limit = 8;
    } else if (1600 < width) {
      limit = 10;
    }

    const { findManyLocation } = await client.FindManyLocation({
      where,
      limit,
      skip,
      sort
    });

    this.data = findManyLocation || [];
  };
}

export default new HomeStore();

import { observer } from 'mobx-react';
import React, { FC } from 'react';
import styled from 'styled-components';

import ListItem from '../../components/ListItem/ListItem';
import SlideBanner from '../../components/SlideBanner/SlideBanner';
import store from './store';

const StyledContainer = styled.div`
  height: 100%;
  background: #fff;
`;

const Home: FC = observer(() => {
  const { data } = store;

  return (
    <StyledContainer>
      <SlideBanner />
      <ListItem title="Sản phẩm mới" data={data} />
    </StyledContainer>
  );
});

export default Home;

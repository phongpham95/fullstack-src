import { Layout, Spin } from 'antd';
import { observer } from 'mobx-react';
import React, { FC, useEffect } from 'react';
import { Route, Switch, useLocation } from 'react-router';

import ErrorBoundary from '../components/Layout/ErrorBoundary';
import Header from '../components/Layout/Header';
import Navigation from '../components/Layout/Navigation';
import ShoppingCart from '../components/ShoppingCart/ShoppingCart';
import { useStores } from '../stores';
import Login from './Login/Login';
import Routes from './Routes';

const { Content } = Layout;

const ScreenRoutes: FC = observer(() => {
  const {
    layoutStore: { isSiderCollapse }
  } = useStores();

  const isMobile = window.innerWidth < 768;

  const content = { marginLeft: 0 };

  if (!isMobile && isSiderCollapse) {
    content.marginLeft = 80;
  } else if (!isMobile && !isSiderCollapse) {
    content.marginLeft = 200;
  }

  return (
    <Layout style={{ ...content }}>
      <Header />
      <Content
        style={{
          minHeight: "calc(100vh - 112px)",
          display: "flex",
          flexFlow: "column",
          margin: "24px 16px",
          background: "#fff"
        }}
      >
        <ErrorBoundary>
          <Routes />
        </ErrorBoundary>
      </Content>
    </Layout>
  );
});

const App: FC = observer(() => {
  const {
    commonStore: { appLoaded, setAppLoaded },
    userStore: { getUser }
  } = useStores();

  const token = localStorage.getItem("token");

  useEffect(() => {
    if (!appLoaded && token) {
      getUser().finally(() => setAppLoaded());
    } else {
      setAppLoaded();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const location = useLocation();

  return (
    <Spin spinning={!appLoaded}>
      <Layout style={{ background: "transparent", height: "100vh" }}>
        {location.pathname !== "/login" && <Navigation />}
        <Switch>
          <Route path="/login" component={Login} />
          <ScreenRoutes />
        </Switch>
        <ShoppingCart />
      </Layout>
    </Spin>
  );
});

export default App;

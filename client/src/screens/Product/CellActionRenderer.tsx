import { Button, Dropdown, Icon, Menu, Popconfirm } from 'antd';
import { observer } from 'mobx-react';
import React, { FC } from 'react';

import store from './store';

const CellActionRenderer: FC<any> = observer(({ text, data }) => {
  const { remove, setModalVisible } = store;

  const moreActions = (
    <Menu>
      <Menu.Item key="1" style={{ color: "red" }}>
        <Popconfirm title="Xóa?" onConfirm={() => remove(text)}>
          <Icon type="delete" />
          Xoá
        </Popconfirm>
      </Menu.Item>
    </Menu>
  );

  return (
    <Button.Group>
      <Button
        icon="edit"
        type="primary"
        onClick={() => setModalVisible(data)}
      />
      <Dropdown overlay={moreActions}>
        <Button icon="ellipsis" type="default" />
      </Dropdown>
    </Button.Group>
  );
});

export default CellActionRenderer;

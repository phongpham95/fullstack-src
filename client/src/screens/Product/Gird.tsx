import { Button } from 'antd';
import { ColumnProps } from 'antd/lib/table';
import TableServer from 'components/Table/TableServer';
import { observer } from 'mobx-react';
import React, { FC } from 'react';
import ReactResizeDetector from 'react-resize-detector';
import { formatTime } from 'utils/formatTime';

import client from '../../graphql/sdk';
import { Location } from '../../graphql/types';
import CellActionRenderer from './CellActionRenderer';
import store from './store';

const columns: ColumnProps<Location>[] = [
  {
    title: (
      <Button
        type="link"
        icon="reload"
        size="small"
        onClick={() => store.refresh()}
      />
    ),
    dataIndex: "_id",
    width: 80,
    ellipsis: true,
    align: "center",
    render: (text, record) => <CellActionRenderer text={text} record={record} />
  },
  {
    title: "Tên địa điểm",
    dataIndex: "name",
    width: 150,
    sorter: true
  },
  {
    title: "Địa chỉ",
    dataIndex: "address",
    width: 200
  },
  {
    title: "Loại địa điểm",
    dataIndex: "categoryId",
    width: 150
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    width: 150,
    render: text => formatTime(text)
  }
];

const ProductGird: FC = observer(() => {
  const { filter } = store;

  return (
    <div style={{ flex: 1, padding: 12 }}>
      <ReactResizeDetector handleHeight>
        {({ height }) => (
          <TableServer
            bodyStyle={{ maxHeight: (height || 300) - 100 }}
            scroll={{ x: 375, y: (height || 300) - 100 }}
            columns={columns}
            onQuery={client.FindManyLocation}
            queryKey="findManyLocation"
            onCount={client.CountLocation}
            countKey="countLocation"
            filterOption={filter}
            size="small"
          />
        )}
      </ReactResizeDetector>
    </div>
  );
});

export default ProductGird;

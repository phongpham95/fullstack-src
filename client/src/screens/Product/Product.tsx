import React, { FC } from 'react';
import styled from 'styled-components';

import ProductForm from './Form';
import ProductGird from './Gird';
import ProductHeader from './Header';

const StyledContainer = styled.div`
  background: #fff;
  display: flex;
  flex-flow: column;
  flex: 1;
`;

const Product: FC = () => (
  <StyledContainer>
    <ProductHeader />
    <ProductGird />
    <ProductForm />
  </StyledContainer>
);

export default Product;

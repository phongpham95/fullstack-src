import { RcCustomRequestOptions, UploadFile } from 'antd/lib/upload/interface';
import axios from 'axios';
import { debounce } from 'lodash';
import { action, observable } from 'mobx';

import { ErrorNoti, SuccessNoti } from '../../components/UI';
import client from '../../graphql/sdk';
import { FindManyLocationQueryVariables, Location } from '../../graphql/types';

export class ProductStore {
  @observable
  selectedItem?: Location = undefined;

  @observable
  filter: FindManyLocationQueryVariables = { limit: 50 };

  @observable
  fileList: UploadFile<any>[] = [];

  @observable
  modalVisible: boolean = false;

  @action
  refresh() {
    this.filter = {
      ...this.filter,
      limit: 50
    };
  }

  @action
  setSearchText = debounce(
    (text?: string) => {
      this.filter = {
        ...this.filter,
        where: { ...this.filter.where, name: text }
      };
    },
    500,
    { leading: false, trailing: true }
  );

  @action
  setModalVisible = (item: Location | undefined = undefined) => {
    if (!item) {
      this.fileList = [];
    } else {
      this.fileList = item!.images!.map(img => {
        const file: UploadFile<any> = {
          uid: img.id,
          size: 0,
          name: img.id,
          type: "",
          url: img.path
        };
        return file;
      });
    }
    this.selectedItem = item;
    this.modalVisible = !this.modalVisible;
  };

  @action
  create = async (record: any) => {
    const {
      createLocation: { error }
    } = await client.CreateLocation({
      record
    });

    if (error) {
      ErrorNoti(error.message);
    } else {
      SuccessNoti("Tạo sản phẩm");
      this.refresh();
    }
  };

  @action
  update = async (_id: string, record: any) => {
    const {
      updateLocationById: { error }
    } = await client.UpdateLocationById({ _id, record });

    if (error) {
      ErrorNoti(error.message);
    } else {
      SuccessNoti("Cập nhật sản phẩm");
      this.refresh();
    }
  };

  @action
  remove = async (_id: string) => {
    const {
      removeLocationById: { error }
    } = await client.RemoveLocationById({
      _id
    });

    if (error) {
      ErrorNoti(error.message);
    } else {
      SuccessNoti("Xoá sản phẩm");
      this.refresh();
    }
  };

  @action
  uploadFile = async ({ onSuccess, onError, file }: RcCustomRequestOptions) => {
    const {
      createFile: { data, error }
    } = await client.CreateFile();

    if (error) {
      return ErrorNoti(error.message);
    }

    const { path: uploadUrl, id } = data!;

    const options = {
      headers: {
        "Content-Type": file.type
      }
    };

    await axios
      .put(uploadUrl, file, options)
      .then(async res => {
        const {
          findFileById: { data }
        } = await client.FindFileById({ id });

        const { path: viewUrl } = data!;

        const fileData: UploadFile<any> = {
          uid: id,
          name: id,
          url: viewUrl,
          type: file.type,
          size: file.size
        };

        this.fileList = [...this.fileList, fileData];

        this.update(this.selectedItem!._id, {
          imageIds: this.fileList.map(f => f.uid)
        });

        onSuccess(res, file);
      })
      .catch(err => {
        onError(err);
      });
  };

  @action
  removeFile = async (file: UploadFile<any>) => {
    const {
      removeFileById: { error }
    } = await client.RemoveFileById({
      id: file.uid
    });

    if (!error) {
      const index = this.fileList.findIndex(f => f.uid === file.uid);

      this.fileList = [
        ...this.fileList.slice(0, index),
        ...this.fileList.slice(index + 1, this.fileList.length)
      ];

      this.update(this.selectedItem!._id, {
        imageIds: this.fileList.map(f => f.uid)
      });
    } else {
      ErrorNoti(error.message);
    }
  };
}

export default new ProductStore();

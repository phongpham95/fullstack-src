import { Button, Icon, Input, PageHeader } from 'antd';
import { observer } from 'mobx-react';
import React, { FC } from 'react';

import store from './store';

const ProductHeader: FC = observer(() => {
  const { setSearchText, setModalVisible } = store;

  return (
    <PageHeader
      title="Danh sách sản phẩm"
      extra={[
        <Input
          key="search"
          suffix={<Icon type="search" />}
          placeholder="Tìm kiếm địa điểm..."
          allowClear={true}
          onChange={e => setSearchText(e.target.value)}
          style={{ width: 180 }}
        />,
        <Button
          key="create"
          type="primary"
          icon="plus"
          onClick={() => setModalVisible()}
        >
          Tạo mới
        </Button>
      ]}
    />
  );
});

export default ProductHeader;

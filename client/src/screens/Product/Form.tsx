import { Form, Icon, Input, Modal, Spin, Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { FormComponentProps } from 'antd/lib/form';
import { UploadProps } from 'antd/lib/upload';
import { get } from 'lodash';
import { observer } from 'mobx-react';
import React, { FC, useState } from 'react';
import { useModalForm } from 'sunflower-antd';

import store from './store';

const { Item } = Form;

const ProductForm: FC<FormComponentProps> = observer(({ form }) => {
  const {
    modalVisible,
    setModalVisible,
    create,
    update,
    uploadFile,
    removeFile,
    selectedItem,
    fileList
  } = store;
  const { getFieldDecorator, resetFields } = form;

  const [uploading, setUploading] = useState(false);

  const { modalProps, formProps, formLoading } = useModalForm({
    form,
    autoResetForm: false,
    async submit(record) {
      if (selectedItem) {
        await update(get(selectedItem, "_id"), record);
        resetFields();
        setModalVisible();
      } else {
        await create(record);
      }

      return "ok";
    }
  });

  const onCancel = () => {
    resetFields();
    setModalVisible();
  };

  const uploadProps: UploadProps = {
    fileList,
    listType: "picture-card",
    customRequest: async option => {
      setUploading(true);
      await uploadFile(option).finally(() => setUploading(false));
    },
    onRemove: file => {
      removeFile(file);
    },
    accept: "image/x-png,image/jpg,image/jpeg"
  };

  const imgCropProps = {
    modalTitle: "Chỉnh sửa ảnh",
    useRatio: true
  };

  const uploadButton = (
    <div>
      <Icon type={uploading ? "loading" : "plus"} />
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  return (
    <Modal
      {...modalProps}
      title="Basic Modal"
      okText={selectedItem ? "Cập nhật" : "Tạo mới"}
      cancelText="Đóng"
      visible={modalVisible}
      onCancel={() => onCancel()}
    >
      <Spin spinning={formLoading}>
        <Form layout="vertical" {...formProps}>
          <Item label="Tên địa điểm">
            {getFieldDecorator("name", {
              rules: [
                { required: true, message: "Vui lòng nhập tên địa điểm!" }
              ],
              initialValue: get(selectedItem, "name") || ""
            })(<Input placeholder="Nhập tên địa điểm" />)}
          </Item>
          <Item label="Địa chỉ">
            {getFieldDecorator("address", {
              rules: [{ required: true, message: "Vui lòng nhập địa chỉ!" }],
              initialValue: get(selectedItem, "address") || ""
            })(<Input placeholder="Nhập địa chỉ" />)}
          </Item>
          <Item label="Loại địa điểm">
            {getFieldDecorator("categoryId", {
              rules: [
                { required: true, message: "Vui lòng nhập loại địa điểm!" }
              ],
              initialValue: get(selectedItem, "categoryId") || ""
            })(<Input placeholder="Nhập loại địa điểm" />)}
          </Item>
          {selectedItem && (
            <ImgCrop {...imgCropProps}>
              <Upload {...uploadProps}>{uploadButton}</Upload>
            </ImgCrop>
          )}
        </Form>
      </Spin>
    </Modal>
  );
});

export default Form.create()(ProductForm);

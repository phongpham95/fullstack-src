import { observer } from 'mobx-react';
import React, { ComponentType, FC, Fragment } from 'react';
import { Redirect, Route, RouteComponentProps, RouteProps } from 'react-router-dom';

import { useStores } from '../stores';
import Dashboard from './Dashboard/Dashboard';
import Home from './Home/Home';
import Product from './Product/Product';

export interface IRoute {
  path: string;
  exact?: boolean;
  isPrivate?: boolean;
  name: string;
  icon?: string;
  component: ComponentType<RouteComponentProps<any>> | ComponentType<any>;
  subRoutes?: IRoute[];
}

export const routes: IRoute[] = [
  {
    path: "/",
    exact: true,
    isPrivate: true,
    name: "Home",
    icon: "home",
    component: Home
  },
  {
    path: "/admin",
    exact: true,
    isPrivate: true,
    name: "Dashboard",
    icon: "dashboard",
    component: Dashboard
  },
  {
    path: "/admin/product",
    isPrivate: true,
    name: "Product",
    icon: "skin",
    component: Product
  }
];

const PrivateRoute: FC<RouteProps> = observer(props => {
  const {
    userStore: { currentUser }
  } = useStores();

  if (currentUser) {
    return <Route {...props} />;
  }

  return (
    <Redirect
      to={{
        pathname: "/login",
        state: { from: props.path }
      }}
    />
  );
});

const Routes = () => (
  <Fragment>
    {routes.map(route =>
      route.isPrivate ? (
        <PrivateRoute key={route.name} {...route} />
      ) : (
        <Route key={route.name} {...route} />
      )
    )}
  </Fragment>
);

export default Routes;

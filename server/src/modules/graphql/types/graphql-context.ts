import { DbLoaderManager } from '@modules/base/dataloader';
import { IAuthInfo } from '@modules/graphql/auth';

export interface IContext {
  authInfo: IAuthInfo;
  loaderManager: DbLoaderManager;
}

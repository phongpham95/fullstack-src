import { ObjectType } from 'type-graphql';

import { StandardMutationError } from '@base/error';
import { Field } from '@modules/graphql/graphql-decorators';
import { User } from '@modules/models/user/user.model';

@ObjectType()
export class UserPayload {
  @Field(() => User, { nullable: true })
  public data?: User;

  @Field(() => StandardMutationError, { nullable: true })
  public error?: StandardMutationError;
}

@ObjectType()
export class LoginPayload extends UserPayload {
  @Field({ nullable: true })
  public authToken?: string;
}

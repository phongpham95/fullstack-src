import { diskStorage } from 'multer';

import {
    Controller, Get, Param, Post, Res, UploadedFile, UploadedFiles, UseInterceptors
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';

import { FilePayload, UploadService } from './upload.service';
import { editFileName, imageFileFilter } from './utils/file-upload';

@Controller('/upload')
export class UploadController {
  constructor(private readonly service: UploadService) {}

  @Post('/')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './resources',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async uploadFile(@UploadedFile() file): Promise<FilePayload> {
    return this.service.uploadFile(file);
  }

  @Post('/multi')
  @UseInterceptors(
    FilesInterceptor('image', 20, {
      storage: diskStorage({
        destination: './resources',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async uploadFiles(@UploadedFiles() files): Promise<FilePayload[]> {
    return this.service.uploadFiles(files);
  }

  @Get(':imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './resources' });
  }
}

import { Injectable } from '@nestjs/common';

export interface FilePayload {
  originalname: string;
  filename: string;
}

@Injectable()
export class UploadService {
  public async uploadFile(file: any): Promise<FilePayload> {
    const res = {
      originalname: file.originalname,
      filename: file.filename,
    };

    return res;
  }

  public async uploadFiles(files: any): Promise<FilePayload[]> {
    const res = [];

    files.forEach(file => {
      const fileRes = {
        originalname: file.originalname,
        filename: file.filename,
      };
      res.push(fileRes);
    });

    return res;
  }
}

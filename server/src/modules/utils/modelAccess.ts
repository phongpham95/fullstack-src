import { intersection, set } from 'lodash';
import { ObjectId } from 'mongodb';

import {
    PERMISSION_READ_ALL, PERMISSION_READ_ALL_OF_CUSTOMER, PERMISSION_SUPERADMIN
} from '@base/decentralization';
import { IAuthInfo } from '@modules/graphql/auth';
import { Owner } from '@modules/models/owner/owner.model';
import { getModelForClass } from '@typegoose/typegoose';

import { enforceQueryId } from './enforceQueryId';

export const handleModelAccess = async (
  where?: any,
  modelName?: string,
  authInfo?: IAuthInfo,
) => {
  if (!authInfo) {
    return where;
  }
  const ownerModel = getModelForClass(Owner);
  const { roles, user, sub } = authInfo;
  const options = {
    owner: true,
    customerId: 'customerId',
    creatorId: 'creatorId',
  };

  if (
    intersection(roles, [PERMISSION_SUPERADMIN, PERMISSION_READ_ALL]).length > 0
  ) {
    return where;
  }

  if (
    user[options.customerId] &&
    (intersection(roles, [`all${modelName}sOfCustomer.read`]).length > 0 ||
      intersection(roles, [PERMISSION_READ_ALL_OF_CUSTOMER]).length > 0)
  ) {
    set(where, [options.customerId], new ObjectId(user[options.customerId]));
    return where;
  }

  const owners = await ownerModel.find({
    model: modelName,
    userId: sub,
  });
  const ownerIds = owners.map(o => new ObjectId(o.resourceId));

  where = enforceQueryId(where, '_id', ownerIds);

  return where;
};

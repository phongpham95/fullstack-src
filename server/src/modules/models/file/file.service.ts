import { minioClient } from '@modules/utils/minio';

export const uploadFileDefaultExpire = 3600;
export const bucketName = 'client';

export class FileService {
  public async createFile() {
    const idWithExt = `file-${Date.now()}`;

    const path = await minioClient.presignedPutObject(
      bucketName,
      idWithExt,
      uploadFileDefaultExpire,
    );

    const data = { path, id: idWithExt };
    return data;
  }

  public async findFileById(id: string) {
    const path = await minioClient.presignedGetObject(bucketName, id);

    const data = { id, path };
    return data;
  }

  public async removeFileById(id: string) {
    await minioClient.removeObject(bucketName, id);

    return { id };
  }
}

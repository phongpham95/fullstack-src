import { InputType, ObjectType } from 'type-graphql';

import { BaseModel } from '@base/base.model';
import { Field } from '@modules/graphql/graphql-decorators';
import { modelOptions, prop } from '@typegoose/typegoose';

@InputType('ProfileInput')
@ObjectType()
@modelOptions({ schemaOptions: { collection: 'Profile' } })
export class Profile extends BaseModel {
  @Field({ filter: true, sort: true })
  @prop({ required: true })
  public profileId: string;

  @Field({ filter: true, sort: true })
  @prop({ required: true })
  public display: string;

  @Field({ nullable: true })
  @prop()
  public description?: string;

  @Field(() => [String])
  @prop({ required: true })
  public roles: string[];
}

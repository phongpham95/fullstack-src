import { createBaseResolver } from '@base/base.resolver';
import {
    PERMISSION_CREATE_LOCATION, PERMISSION_DELETE_LOCATION, PERMISSION_READ_ALL,
    PERMISSION_READ_ALL_LOCATIONS, PERMISSION_READ_ALL_LOCATIONS_OF_CUSTOMER,
    PERMISSION_READ_LOCATION, PERMISSION_UPDATE_LOCATION
} from '@base/decentralization';

import { Location } from './location.model';

const { BaseResolver: BaseLocationResolver } = createBaseResolver<Location>(
  Location,
  {
    action: {
      read: {
        role: [
          PERMISSION_READ_LOCATION,
          PERMISSION_READ_ALL_LOCATIONS,
          PERMISSION_READ_ALL_LOCATIONS_OF_CUSTOMER,
          PERMISSION_READ_ALL,
        ],
      },
      create: { role: [PERMISSION_CREATE_LOCATION] },
      update: { role: [PERMISSION_UPDATE_LOCATION] },
      remove: { role: [PERMISSION_DELETE_LOCATION] },
    },
  },
);

export { BaseLocationResolver };

import Promise from 'bluebird';
import { Ctx, FieldResolver, Resolver, Root } from 'type-graphql';
import { Container } from 'typedi';

import { IContext } from '@graphql/types/graphql-context';
import { File } from '@models/file/file.model';
import { FileService } from '@models/file/file.service';
import { User } from '@models/user/user.model';
import { ReturnModelType } from '@typegoose/typegoose';

import { BaseLocationResolver } from './location.base';
import { Location } from './location.model';
import { LocationService } from './location.service';

@Resolver(Location)
export class LocationResolver extends BaseLocationResolver {
  constructor(
    private readonly service: LocationService,
    private readonly fileService: FileService,
  ) {
    super();
    Container.set(LocationResolver, this);
  }
  @FieldResolver(() => User, { nullable: true })
  public async creator(
    @Root() location: ReturnModelType<typeof Location>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<User> {
    if (!location.creatorId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(location.creatorId.toString());
  }

  @FieldResolver(() => User, { nullable: true })
  public async customer(
    @Root() location: ReturnModelType<typeof Location>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<User> {
    if (!location.customerId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(location.customerId.toString());
  }

  @FieldResolver(() => [File], { nullable: true })
  public async images(
    @Root() location: ReturnModelType<typeof Location>,
  ): Promise<File[]> {
    if (!location.imageIds) {
      return;
    }

    const data = await Promise.map(location.imageIds, async imageId =>
      this.fileService.findFileById(imageId),
    );

    return data;
  }
}

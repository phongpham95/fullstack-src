import { compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { ObjectId } from 'mongodb';
import { InjectModel } from 'nestjs-typegoose';

import { BaseService } from '@base/base.service';
import { existUser, invalidLogin, invalidMutation } from '@base/error';
import { IAuthInfo, jwtDefaultExpire, jwtSecret } from '@graphql/auth';
import { LoginPayload, UserPayload } from '@graphql/payload';
import { RequestContext } from '@modules/context';
import { Profile } from '@modules/models/profile/profile.model';
import { Role } from '@modules/models/role/role.model';
import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';

import { User } from './user.model';

@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @InjectModel(User)
    protected readonly model: ReturnModelType<typeof User>,
    @InjectModel(Profile)
    protected readonly profileModel: ReturnModelType<typeof Profile>,
  ) {
    super();
  }

  private signToken(user: User, roles: Role[]) {
    return sign(
      {
        user,
        roles,
      },
      Buffer.from(jwtSecret, 'base64'),
      {
        subject: user._id.toString(),
        expiresIn: jwtDefaultExpire,
      },
    );
  }

  public async login(email: string, password: string): Promise<LoginPayload> {
    let roles = [];

    const user = await this.model
      .findOne({
        email,
      })
      .exec();
    if (!user) {
      return invalidLogin;
    }

    const valid = await compare(password, user.password);
    if (!valid) {
      return invalidLogin;
    }

    const profile = await this.profileModel
      .findOne({
        profileId: user.profileId,
      })
      .exec();
    if (!profile) {
      return invalidLogin;
    }

    roles = profile.roles;
    const authToken = this.signToken(user, roles);

    return {
      authToken,
      data: user,
    };
  }

  public async createUser(record: User): Promise<UserPayload> {
    const { email, profileId } = record;

    const user = await this.model
      .findOne({
        email,
      })
      .exec();
    if (user) {
      return existUser;
    }

    const profile = await this.profileModel.findOne({ profileId }).exec();
    if (!profile) {
      return invalidMutation;
    }

    const createdUser = await this.model.create({
      ...record,
    });
    if (!createdUser) {
      return invalidMutation;
    }

    return {
      data: createdUser,
    };
  }

  public async findMe(): Promise<UserPayload> {
    const { sub }: IAuthInfo = await RequestContext.authInfo();

    const user = await this.model.findById(sub).exec();
    if (!user) {
      return invalidMutation;
    }

    return {
      data: user,
    };
  }

  public async changePassword(
    oldPassword: string,
    newPassword: string,
  ): Promise<UserPayload> {
    const { sub }: IAuthInfo = await RequestContext.authInfo();

    const user = await this.model.findById(new ObjectId(sub)).exec();
    if (!user) {
      return invalidMutation;
    }

    const valid = await compare(oldPassword, user.password);
    if (!valid) {
      return invalidMutation;
    }

    Object.assign(user, { password: newPassword });
    const r = await user.save();

    return {
      data: r,
    };
  }
}

import { Arg, Authorized, Ctx, FieldResolver, Query, Resolver, Root } from 'type-graphql';
import { Container } from 'typedi';

import { PERMISSION_AUTHENTICATED, PERMISSION_CREATE_USER } from '@base/decentralization';
import { Mutation } from '@graphql/graphql-decorators';
import { LoginPayload, UserPayload } from '@graphql/payload';
import { IContext } from '@graphql/types/graphql-context';
import { Profile } from '@modules/models/profile/profile.model';
import { ReturnModelType } from '@typegoose/typegoose';

import { BaseUserResolver } from './user.base';
import { User } from './user.model';
import { UserService } from './user.service';

@Resolver(User)
export class UserResolver extends BaseUserResolver {
  constructor(private readonly service: UserService) {
    super();
    Container.set(UserResolver, this);
  }
  @FieldResolver(() => Profile)
  public async profile(
    @Root() user: ReturnModelType<typeof User>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<Profile> {
    if (!user.profileId) {
      return;
    }
    const loader = loaderManager.getLoader(Profile, 'profileId');
    return loader.load(user.profileId);
  }

  @FieldResolver(() => User, { nullable: true })
  public async creator(
    @Root() user: ReturnModelType<typeof User>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<User> {
    if (!user.creatorId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(user.creatorId.toString());
  }

  @FieldResolver(() => User, { nullable: true })
  public async customer(
    @Root() user: ReturnModelType<typeof User>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<User> {
    if (!user.customerId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(user.customerId.toString());
  }

  @FieldResolver()
  protected password(): string {
    return 'Password is hashed !';
  }

  @Mutation(() => LoginPayload)
  public async login(
    @Arg('email') email: string,
    @Arg('password') password: string,
  ) {
    return this.service.login(email, password);
  }

  @Authorized(PERMISSION_CREATE_USER)
  @Mutation(() => UserPayload)
  public async createUser(@Arg('record') record: User) {
    return this.service.createUser(record);
  }

  @Authorized(PERMISSION_AUTHENTICATED)
  @Query(() => UserPayload)
  public async me() {
    return this.service.findMe();
  }

  @Authorized(PERMISSION_AUTHENTICATED)
  @Mutation(() => UserPayload)
  public async changePassword(
    @Arg('oldPassword') oldPassword: string,
    @Arg('newPassword') newPassword: string,
  ) {
    return this.service.changePassword(oldPassword, newPassword);
  }
}

import { hashSync } from 'bcrypt';
import { ObjectId } from 'mongodb';
import { ID, InputType, ObjectType } from 'type-graphql';

import { BaseModel } from '@base/base.model';
import { ancestorPlugin, modelAccessPlugin } from '@database/plug-in';
import { Field } from '@modules/graphql/graphql-decorators';
import { modelOptions, plugin, post, pre, prop } from '@typegoose/typegoose';

@post<User>('save', async function() {
  if (this.profileId !== 'superadmin' && !this.customerId) {
    this.customerId = this._id;
    await this.save();
  }
})
@pre<User>('save', async function() {
  if (this.isModified('password') || this.isNew) {
    this.password = hashSync(this.password, 10);
  }
})
@InputType('UserInput')
@ObjectType()
@plugin(modelAccessPlugin)
@plugin(ancestorPlugin)
@modelOptions({ schemaOptions: { collection: 'User' } })
export class User extends BaseModel {
  @Field({ filter: true, sort: true })
  @prop({ required: true })
  public name: string;

  @Field({ filter: true, sort: true })
  @prop({ required: true, unique: true })
  public email: string;

  @Field({ filter: true, sort: true, nullable: true })
  @prop()
  public phone?: string;

  @Field({})
  @prop({ required: true })
  public password: string;

  @Field({ filter: true, sort: true })
  @prop({ required: true })
  public profileId: string;

  @Field(() => ID, { nullable: true, filter: true, sort: true })
  @prop()
  public creatorId?: ObjectId;

  @Field(() => ID, { nullable: true, filter: true, sort: true })
  @prop()
  public customerId?: ObjectId;
}

import { InjectModel } from 'nestjs-typegoose';
import { Container } from 'typedi';

import { BaseService } from '@base/base.service';
import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';

import { Owner } from './owner.model';

@Injectable()
export class OwnerService extends BaseService<Owner> {
  constructor(
    @InjectModel(Owner)
    protected readonly model: ReturnModelType<typeof Owner>,
  ) {
    super();
    Container.set(OwnerService, this);
  }
}

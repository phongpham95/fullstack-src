import { TypegooseModule } from 'nestjs-typegoose';

import { Module } from '@nestjs/common';

import { Owner } from './owner.model';
import { OwnerService } from './owner.service';

@Module({
  imports: [TypegooseModule.forFeature([Owner])],
  providers: [OwnerService],
})
export class OwnerModule {}

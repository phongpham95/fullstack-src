import { ObjectType } from 'type-graphql';

import { BaseModel } from '@base/base.model';
import { Field } from '@modules/graphql/graphql-decorators';
import { modelOptions, prop } from '@typegoose/typegoose';

@ObjectType()
@modelOptions({ schemaOptions: { collection: 'Role' } })
export class Role extends BaseModel {
  @Field({ filter: true, sort: true })
  @prop({ required: true })
  public roleId: string;

  @Field({ nullable: true })
  @prop()
  public description?: string;
}

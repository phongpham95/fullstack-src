import { User } from '@modules/models/user/user.model';
import { getModelForClass } from '@typegoose/typegoose';

export const su = {
  name: 'SUPERADMIN',
  email: 'su@meganet.com.vn',
  password: '123456',
  profileId: 'superadmin',
};

export const createSu = async () => {
  const userModel = getModelForClass(User);
  const user = await userModel.findOne({
    email: su.email,
  });
  if (!user) {
    await userModel.create(su);
  } else {
    Object.assign(user, { password: su.password });
    await user.save();
  }
};

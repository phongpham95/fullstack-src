import '../../alias';
import 'reflect-metadata';

import Promise from 'bluebird';
import { uniqBy } from 'lodash';
import { ObjectId } from 'mongodb';
import { shim } from 'promise.prototype.finally';

import { AppModule } from '@/app.module';
import { LocationModule } from '@models/location/location.module';
import { LocationService } from '@models/location/location.service';
import { NestFactory } from '@nestjs/core';

import { data } from './MOCK_DATA';

shim();

async function genarateData() {
  const app = await NestFactory.create(AppModule);
  const locationService = app.select(LocationModule).get(LocationService);

  const uniqData = uniqBy(data, 'name');

  await Promise.each(uniqData, async l => {
    await locationService.create({
      ...l,
      creatorId: new ObjectId("5dd67ccdae25400556f4fc8e"),
    });
  });
}

genarateData()
  // tslint:disable-next-line:no-console
  .catch(console.log)
  .finally(() => process.exit(0));

import { get } from 'lodash';
import { ObjectId } from 'mongodb';
import { Schema } from 'mongoose';

import { Owner } from '@models/owner/owner.model';
import { RequestContext } from '@modules/context';
import { getModelForClass } from '@typegoose/typegoose';

import { handleModelAccess } from '../modules/utils/modelAccess';

export const modelAccessPlugin = (schema: Schema) => {
  const processOwner = async function() {
    let where = this.getQuery();
    const modelName = this.model.modelName;

    const accessToken = this.options.accessToken;
    if (accessToken) {
      where = await handleModelAccess(where, modelName, accessToken);
    }

    this.setQuery(where);
  };

  schema.pre('count', processOwner);
  schema.pre('find', processOwner);
  schema.pre('findOne', processOwner);
  schema.pre('findOneAndRemove', processOwner);
  schema.pre('findOneAndUpdate', processOwner);
  schema.pre('update', processOwner);
  schema.pre('updateOne', processOwner);
  schema.pre('updateMany', processOwner);
};

export const ancestorPlugin = (schema: Schema) => {
  const processAncestor = async function() {
    const authInfo = await RequestContext.authInfo();
    if (!authInfo) {
      return;
    }

    if (!this.creatorId) {
      this.creatorId = authInfo && new ObjectId(authInfo.sub);
    }

    if (!this.customerId) {
      const customerId = get(authInfo, 'user.customerId');
      this.customerId = customerId && new ObjectId(customerId);
    }
  };

  schema.pre('save', processAncestor);
};

export const ownerPlugin = (schema: Schema) => {
  const ownerModel = getModelForClass(Owner);

  const createOwner = async function() {
    const owner = {
      userId: this.creatorId.toString(),
      model: get(schema, 'options.collection'),
      resourceId: this._id.toString(),
    };

    const existedOwner = await ownerModel.findOne(owner);
    if (existedOwner) {
      return;
    }

    await ownerModel.create(owner);
  };

  const removeOwner = async function() {
    const owner = {
      userId: this.creatorId.toString(),
      model: get(schema, 'options.collection'),
      resourceId: this._id.toString(),
    };

    const existedOwner = await ownerModel.findOne(owner);
    if (!existedOwner) {
      return;
    }

    await existedOwner.remove();
  };

  schema.post('save', createOwner);
  schema.post('remove', removeOwner);
};

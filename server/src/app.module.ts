import '@modules/graphql/replace-metadata';

import { MongoModule } from '@database/mongo.module';
import { FileModule } from '@models/file/file.module';
import { LocationModule } from '@models/location/location.module';
import { RequestContextMiddleware } from '@modules/context';
import { GraphQlModule } from '@modules/graphql/graphql.module';
import { OwnerModule } from '@modules/models/owner/owner.module';
import { ProfileModule } from '@modules/models/profile/profile.module';
import { RoleModule } from '@modules/models/role/role.module';
import { UserModule } from '@modules/models/user/user.module';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

// import { UploadModule } from '@modules/upload/upload.module';

@Module({
  imports: [
    MongoModule,
    GraphQlModule,
    // UploadModule,
    UserModule,
    ProfileModule,
    RoleModule,
    OwnerModule,
    LocationModule,
    FileModule,
  ],
})
export class AppModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestContextMiddleware).forRoutes('*');
  }
}
